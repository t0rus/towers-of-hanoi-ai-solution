/*
  Towers of Hanoi AI Solution
  
   Author: Taylor Bockman


  */

import scala.collection.immutable.{Stack, Queue}


//This is our state-space representation. A 4-tuple would also work, but
//having something named 'Position' is a little easier to reason about.
//
//  disk1 = Position of disk 1
//  disk2 = Position of disk 2
//  disk3 = Position of disk 3
//
//We will build a tree out of these positions to solve the problem using DFS.
//
//Note: children is mutable mostly for the reason it vastly simplifies the tree building
//process. If we didn't do that we would have to spit out an entirely new tree every time
//a new node was created.
//
case class Position(disk1: Int, disk2: Int, disk3: Int, var children: Seq[Position])


class TowersOfHanoi{


  //buildMoves
  //
  //buildMoves constructs all possible moves based on whether or
  //not that move is valid. Takes a parameter that represents a list of seen
  //moves, as well as the parent position, p.
  //
  //buildMoves returns a 2-tuple consisting of the first list, which is the 
  //list containing the valid moves, and the second list, which is the list
  //of moves seen.
  //
  def buildMoves(p: Position, seen: List[Position]): (List[Position], List[Position]) = {
    
    //Generate a list of moves
    val tempMoves: List[Option[Position]] = List[Option[Position]](
        move(p, 1, 1),
        move(p, 1, 2),
        move(p, 1, 3),
        move(p, 2, 1),
        move(p, 2, 2),
        move(p, 2, 3),
        move(p, 3, 1),
        move(p, 3, 2),
        move(p, 3, 3)
      )

    //Flatten the temporary moves list to get rid of Nones (bad moves)
    //Then filter out those results based on our seen list.
    val validMoves = tempMoves.flatten.filterNot(b => seen.exists(a => a.disk1 == b.disk1 && a.disk2 == b.disk2 && a.disk3 == b.disk3))
    val newSeen = validMoves.foldLeft(seen)((b,a) => b :+ a)
    (validMoves, newSeen)
  }

  /*
    Move executes a move from the parent. It returns an optional child.

    The idea is to create a list of all possible moves using move, and then
    flattening the list so that only the moves that obey the rules (non-None)
    remain.

    Note:
      - No matter what, if n disks are on pole p then the order must be
        disk 1 first, disk 2 second, disk 3 third ALWAYS for this to be valid
        per rule #2.
      - We assume the previous move was also valid.
  */
  def move(parent: Position, disk: Int, pole: Int): Option[Position] = {
    if(disk == 1){
      //Disk one must always be on top, so it can effectively move anywhere.
      Some(Position(pole, parent.disk2, parent.disk3, List[Position]()))
    } else if(disk == 2){
      //We need to make sure disk 2 isn't below a disk 1
      if(parent.disk1 == parent.disk2){
        //Disk 2 is under disk 1 and cannot be moved. Invalid.
        None
      } else {
        //Disk 2 is not under disk 1, move anywhere that isn't disk 1
        if(parent.disk1 == pole){
          None
        } else {
          Some(Position(parent.disk1, pole, parent.disk3, List[Position]()))
        }
      }
    } else {
      //Disk 3 - We need to make sure disk 3 isn't below disk 1 or disk 2 first
      if(parent.disk1 == parent.disk3 || parent.disk2 == parent.disk3){
        //Disk 3 is below either disk 1 or disk 2 - Invalid
        None
      } else {
        //We can move disk 3 anywhere where disk 1 and disk 2 are not
        if(parent.disk1 == pole || parent.disk2 == pole){
          //Trying to move disk 3 to a place where there is a smaller disk is invalid
          None
        } else {
          Some(Position(parent.disk1, parent.disk2, pole, List[Position]()))
        }
      }
    }
  }



  //Builds our decision tree using BFS.
  def buildTree(): Position = {

    //Starting position is always all 3 on 1
    val root = Position(1,1,1, List[Position]())

    //Create list of previous moves...
    val seen = List[Position](root)

    @annotation.tailrec
    def innerBuild(q: Queue[Position], seen: List[Position]): Unit = q.dequeueOption match {
      case Some((p: Position, qs: Queue[Position])) => {
        

        //build children
        val childrenSet = buildMoves(p, seen)
        p.children = childrenSet._1

        //Move children into the queue. Only put ones that are not the winning combination.
        val newQueue = p.children.filterNot(a => a.disk1 == 3 && a.disk2 == 3 && a.disk3 == 3).foldLeft(qs)((b,a) => b :+ a)

        //Call innerBuild with our new list.
        innerBuild(newQueue, childrenSet._2)



      }

      case None => () //Do nothing, job done.

    }

    innerBuild(Queue[Position](root), seen)
    root
  }


  //Solve the problem using DFS. Return a list of moves required to 
  //win the game.
  def solve(root: Position): List[Position] = {

    def innerSolve(s: Stack[Position], backtrack: Queue[Position]): List[Position] = {
      if(s.size > 0){
        val top = s.pop2
        val n = top._1
        val rs = top._2

        //If we have seen the node skip it entirely
        if(backtrack.exists(a => a.disk1 == n.disk1 && a.disk2 == n.disk2 && a.disk3 == n.disk3)){
          innerSolve(rs, backtrack)
        } else {
          //The node hasn't been visited yet, go ahead and continue processing
          if(n.disk1 == 3 && n.disk2 == 3 && n.disk3 == 3){
            //Solution found!
            (backtrack :+ n).toList
          } else {
            //No solution yet, keep looking.
            if(n.children.size == 0){
              //If it has no children we cannot use this in our solution.
              //Drop the last position to be put in the queue and continue.
              innerSolve(rs, backtrack.dropRight(1))
            } else {
              val newStack = n.children.foldLeft(rs)((b,a) => b.push(a))
              innerSolve(newStack, backtrack :+ n)
            }
          }

        }
      } else {
        //Stack emptied before a solution was found.
        List[Position]()
      } 
    }

    innerSolve(Stack[Position](root), Queue[Position]())
  }


}


object Main{
  def main(args: Array[String]) = {
    println("Building Tree... ")
    val toh = new TowersOfHanoi()
    val tree = toh.buildTree
    println("Solving... ")
    val solution = toh.solve(tree)
    println("Solution: ")
    println("Each move represents which tower to place the disk on. The format")
    println("is (disk 1 position, disk 2 position, disk 3 position)")
    solution.foreach(s => println("(" + s.disk1 + ", " + s.disk2 + ", " + s.disk3 + ")"))
  }
}